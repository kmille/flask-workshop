#!/usr/bin/env python3
from flask import Flask, render_template, request, make_response, session
from ipdb import set_trace
import os

app = Flask(__name__)
app.secret_key = 'any random string'
lesezeichen = "lesezeichen.txt"

@app.route("/")
def index():
    return render_template("index.html")


@app.route("/add", methods=['POST'])
def add():
    url = request.form['url']
    with open(lesezeichen, "a") as f:
        f.write(url + "\n")
    resp = make_response(render_template("add.html", url=url))
    resp.set_cookie("last", url)
    return resp


@app.route("/list")
def list():
    if not 'counter' in session:
        session['counter'] = 1
    else:
        session['counter'] += 1
    with open(lesezeichen, "r") as f:
        data = f.read()
    return render_template("list.html", data=data)
    
@app.route("/last")
def last():
    last_url = request.cookies.get("last")
    return render_template("last.html", last_url=last_url)
    

if __name__ == '__main__':
    app.run(debug=True)
